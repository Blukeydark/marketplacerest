FROM php:7.2-apache

RUN apt-get update \
 && apt-get install -y git zlib1g-dev libicu-dev g++ \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6-dev \
 && docker-php-ext-install zip \
 && docker-php-ext-install mysqli \
 && docker-php-ext-install pdo_mysql \
 && docker-php-ext-install intl \
 && docker-php-ext-configure intl \
 && docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir \
 && docker-php-ext-install gd \
 && docker-php-ext-install exif \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/webroot!g' /etc/apache2/sites-enabled/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer


EXPOSE 8080:8080


WORKDIR /var/www
