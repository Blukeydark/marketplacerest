# CakePHP Application Skeleton Test MarketPlace BE


## The project was created with cakephp 3.9 from the command below:
```bash
composer create-project --prefer-dist cakephp/app:^3.9 marketplacerest
```


## The servers running on port '8080' (php-apache) and '3306', turn off the services on this port while  the tests are running
## For running test pull down the image servers with the docker-compose
```bash
docker-compose  up -d --build
```


## After the docker-compose, check the status of services:
```bash
docker-compose  ps
```


## OUTPUT:
```
   Name                  Command               State               Ports
-------------------------------------------------------------------------------------
marketPlace   docker-php-entrypoint apac ...   Up      0.0.0.0:8080->80/tcp, 8080/tcp
mysql         docker-entrypoint.sh --def ...   Up      3306/tcp, 33060/tcp
```


## Now we can connect to php-apache server with the command:
```bash
docker exec -it marketPlace /bin/bash
```

## Download and install vendor package:
```bash
root@dc573badd2de:/var/www# composer install
```



## We are logged in php-apache/server as 'root' user with workdir->'/var/www':
```bash
root@72ac2f75aa97:/var/www#
```

## Change /var/www/ directory permissions
```bash
root@dc573badd2de:/var/www# chmod -R 775 /var/www/
```


## Put your CSV file (or leave what's inside) in <project-path>/db/data/flyers_data.csv and run the command below:
```bash
root@72ac2f75aa97:/var/www# bin/cake migrations migrate
```
The mogrations create the table-entity "Flyers" and copy the CSV rows into table



## Change LOGS directory permissions
```bash
root@dc573badd2de:/var/www# chmod -R 777 logs/
root@dc573badd2de:/var/www# chmod -R 777 tmp/
```

## now we are able to navigate:

## ENDPOINT
http://localhost:8080/rest-market-place/flyers.json
http://localhost:8080/rest-market-place/flyers/1.json

## try querystring filter
http://localhost:8080/rest-market-place/flyers.json?page=1&limit=100&filter[is_published]=1
http://localhost:8080/rest-market-place/flyers.json?page=1&limit=100&fields=id,title,category&filter[is_published]=0









