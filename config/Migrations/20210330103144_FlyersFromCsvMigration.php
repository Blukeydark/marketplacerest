<?php
use Migrations\AbstractMigration;

class FlyersFromCsvMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * @return void
     */
    public function change()
    {
        try{
            $this->execute($this->queryCreate);
            $filename = getcwd() . '/db/data/flyers_data.csv';
            $delim = ",";
            if (!file_exists($filename)) {
                throw new \Exception("The file $filename NOT exists");
            }
            $handle = fopen($filename, "r");
            $header = fgetcsv($handle,null,$delim);
            $queryInsert="";
            while (($data = fgetcsv($handle,null,$delim)) !== FALSE) {
                    $qi = "";
                    foreach ($header as $key=>$heading) {
                        $qi = (!strlen($qi))?"(":$qi . ",";
                        /*
                         * for entities insert
                         * $heading = trim($heading);
                         * $row[$heading]=(isset($data[$key])) ? $data[$key] : '';
                         */
                        $qi .= '"'.((isset($data[$key])) ? $data[$key] : '') . '"';
                    }

                    $queryInsert .= "\n" . $qi ."),";
            }
            fclose($handle);


            $q = "INSERT INTO `flyers` "
                . "(`id`,`title`, `start_date`,  `end_date`, `is_published`, `retailer`, `category`)"
                . "VALUES" . "\n";

            $this->execute($q . substr($queryInsert, 0, -1));


        } catch (\Exception $ex) {
            throw new \Exception("Errore durante la migrazione: " . $ex->getMessage());
        }
    }

    /**
     *  DDL Flyers
     *
     * @var type
     */
    private $queryCreate = "CREATE TABLE  IF NOT EXISTS `flyers` ( "
            . "`id` int(11) NOT NULL AUTO_INCREMENT, "
            . " `title` varchar(100) NOT NULL, "
            . "`start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "
            . "`end_date` timestamp NULL DEFAULT NULL, "
            . "`is_published` char(1) NOT NULL, "
            . "`retailer` varchar(50) NOT NULL, "
            . " `category` varchar(50) NOT NULL, "
            . " PRIMARY KEY (`id`), "
            . "KEY `is_published` (`is_published`), "
            . "KEY `category` (`category`), "
            . "KEY `start_date` (`start_date`), "
            . "KEY `end_date` (`end_date`) "
            . ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

}
