<?php
namespace RestMarketPlace\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use RestMarketPlace\Model\Table\FlyersTable;

/**
 * RestMarketPlace\Model\Table\FlyersTable Test Case
 */
class FlyersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \RestMarketPlace\Model\Table\FlyersTable
     */
    public $Flyers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.RestMarketPlace.Flyers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Flyers') ? [] : ['className' => FlyersTable::class];
        $this->Flyers = TableRegistry::getTableLocator()->get('Flyers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Flyers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
