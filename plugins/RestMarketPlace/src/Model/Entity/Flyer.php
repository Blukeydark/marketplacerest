<?php
namespace RestMarketPlace\Model\Entity;

use Cake\ORM\Entity;

/**
 * Flyer Entity
 *
 * @property int $id
 * @property string $title
 * @property \Cake\I18n\FrozenTime $start_date
 * @property \Cake\I18n\FrozenTime|null $end_date
 * @property string $is_published
 * @property string $retailer
 * @property string $category
 */
class Flyer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'title' => true,
        'start_date' => true,
        'end_date' => true,
        'is_published' => true,
        'retailer' => true,
        'category' => true,
    ];
    
    
    
    
}
