<?php
namespace RestMarketPlace\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use RestMarketPlace\Error\Exception\MarketPlaceException;
use RestMarketPlace\Model\Entity\Flyer;
use function false;

/**
 * Flyers Model
 *
 * @method Flyer get($primaryKey, $options = [])
 * @method Flyer newEntity($data = null, array $options = [])
 * @method Flyer[] newEntities(array $data, array $options = [])
 * @method Flyer|false save(EntityInterface $entity, $options = [])
 * @method Flyer saveOrFail(EntityInterface $entity, $options = [])
 * @method Flyer patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Flyer[] patchEntities($entities, array $data, array $options = [])
 * @method Flyer findOrCreate($search, callable $callback = null, $options = [])
 */
class FlyersTable extends Table
{
    
    
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('flyers');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }
    
    
    
    public function findById($fields, $id)
    {
        $fieldSelected=$this->getFilterFields($fields); 
        
        $flyer =$this->find()
            ->select($fieldSelected)
             ->where(
            [
                'id'=>$id
        ]);
        return $flyer;
    }
    
    
    
    
    /**
     * 
     * @param [] $fields
     * @param int $is_published
     * @param [] $filters
     * @return type
     * @throws MarketPlaceException
     */
    public function findAllFlyers($fields, $is_published = 1, $filters = [])
    {
        $fieldSelected=$this->getFilterFields($fields); 
        
        $currentDate = date("Y-m-d");
        
        $whereCond = [
            'start_date <='=>$currentDate,
            'end_date >='=>$currentDate,
            'is_published'=>$is_published
        ]; 
        if($filters !== null && is_array($filters) && count($filters)>0){
            
             
            $countCond = 0;
            if(in_array("is_published", array_keys($filters)))
            {
                // Cast to int (rewrite input)
                $whereCond["is_published"]=intval($filters["is_published"]);
                $countCond++;
            }
            if(
                    in_array("category", array_keys($filters)) && 
                    strlen($filters["category"]) < 50 &&
                    !preg_match('/[^A-Za-z0-9 ]/', $filters["category"])
                    )
            {
                // string contains only letters, space & digits
                 $whereCond["category"]=$filters["category"];
                $countCond++;
            }
            
            
            if(count($filters) != $countCond){
                throw new MarketPlaceException("Not Allowed filter: {{wrong filter list}}", 400);
            }
        } 
        
        $query =$this->find() 
            ->select($fieldSelected)
            ->where($whereCond)
            ->order(['id'=>'ASC']);
        return $query;
    }
    
    /**
     * 
     * @param type $fields
     * @return string
     * @throws MarketPlaceException
     */
    private function getFilterFields($fields)
    {
        $allowed = [
            "id",	
            "title",	
            "start_date",	
            "end_date",	
            "is_published",	
            "retailer",	
            "category",
        ];
        
        $output=[];
        
        if($fields === null || empty($fields))
        {
            return $allowed;
        }
        
        $field  = explode(",", $fields);  
        
        
        if($field !== null && count($field) > 0){
            foreach($field as $f){
                $key = array_search(trim($f), $allowed);
                if($key !== false){
                    $output[$key]=$allowed[$key];
                }
            }
            if(!count($output)){
                throw new MarketPlaceException("Not Allowed fields: {{wrong field list separed by comma}}", 400);
            }
            return $output;
        }
        
        return $allowed;
    }
    
    
    
    
    

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 100)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->dateTime('start_date')
            ->notEmptyDateTime('start_date');

        $validator
            ->dateTime('end_date')
            ->allowEmptyDateTime('end_date');

        $validator
            ->scalar('is_published')
            ->maxLength('is_published', 1)
            ->requirePresence('is_published', 'create')
            ->notEmptyString('is_published');

        $validator
            ->scalar('retailer')
            ->maxLength('retailer', 50)
            ->requirePresence('retailer', 'create')
            ->notEmptyString('retailer');

        $validator
            ->scalar('category')
            ->maxLength('category', 50)
            ->requirePresence('category', 'create')
            ->notEmptyString('category');

        return $validator;
    }
}
