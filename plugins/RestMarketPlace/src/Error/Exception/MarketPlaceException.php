<?php

namespace RestMarketPlace\Error\Exception;

use Cake\Http\Exception\HttpException;

class MarketPlaceException extends HttpException
{

    public function __construct($message = null, $code = 400)
    {
        if ($message === null) {
            $message = 'A generic error occurred.';
        }
        parent::__construct($message, $code);
    }

}