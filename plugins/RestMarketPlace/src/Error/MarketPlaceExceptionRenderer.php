<?php

namespace RestMarketPlace\Error;

use Cake\Error\ExceptionRenderer;


class MarketPlaceExceptionRenderer extends ExceptionRenderer{
    
    public function missingController($error){
        $response = $this->controller->response;
        $codeError = $error->getCode();
        try{        
            
            $viewVars = [
                "success"=>false,
                "code"=>$codeError,
                "error"=>[
                    "message"=>"File not found",
                    "debug"=>"Not Found: {{" . $error->getMessage() ."}}"
                ]
            ];           
            
            $this->controller->set($viewVars);            
            $this->controller->viewBuilder()->setClassName('RestMarketPlace.Json');
            $this->controller->render();
            return $this->controller->response;
        } catch (\Exception $ex) {
            return $response->withStringBody('Oops that generic is missing.' . $ex->getMessage());            
        }
    }
    
    public function marketPlace($error){
         
        $response = $this->controller->response;
        $codeError = $error->getCode();
        try{        
            
            $viewVars = [
                "success"=>false,
                "code"=>$error->getCode(),
                "error"=>[
                    "message"=>$codeError < 500?"Bad Request":"Internal Server Error",
                    "debug"=>$error->getMessage()
                ]
            ];           
            
            $this->controller->set($viewVars);
            $this->controller->set('_serialize', true); 
            $this->controller->render();
            return $this->controller->response;
        } catch (\Exception $ex) {
            return $response->withStringBody('Oops that generic is missing.' . $ex->getMessage());            
        }
    }
    
}
