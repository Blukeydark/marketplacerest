<?php
namespace RestMarketPlace\Controller;

use RestMarketPlace\Controller\AppController;
use Cake\Event\Event;
use RestMarketPlace\Error\Exception\MarketPlaceException;
use \Exception;


/**
 * Flyers Controller
 *
 *
 * @method \RestMarketPlace\Model\Entity\Flyer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FlyersController extends AppController
{
    
    
    /**
     * RESTFul 
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    
    /**
     * Only JSON Content
     * @param Event $event
     */
    public function beforeRender(Event $event)
    {
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
        $this->set('_serialize', true);
        
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {           
        try{
            // URL is flyers.json?page=2&limit=100&fields=id,title
            $filters = $this->request->getQuery('filter');  


            // URL is flyers.json?page=2&limit=100&fields=id,title
            $fields = $this->request->getQuery('fields');  
            $flyers = $this->paginate( $this->Flyers->findAllFlyers($fields,  1, $filters) );   
 
            if(null == $flyers || !$flyers->count()){
                throw new MarketPlaceException("Not Found", 404);
            }

            $this->set('results', $flyers);
            $this->set('success', true);
            $this->set('code', 200);
            
        } catch (MarketPlaceException $ex) {
            throw $ex;   
        } catch (\Cake\Http\Exception\NotFoundException $ex) {
            throw new MarketPlaceException("Not Found", 404);  
        } catch (Exception $ex) {
            throw new MarketPlaceException(
                    "Flyers generic error: " . $ex->getMessage(), 500);   
        }      

    }

    /**
     * View method
     *
     * @param string|null $id Flyer id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {      
        try{
            // URL is flyers.json?page=2&limit=100&fields=id,title
            $fields = $this->request->getQuery('fields');
            /**
             * @param \Cake\ORM\Query
             */
            $flyer =$this->Flyers->findById($fields, intval($id)); 

            if(null == $flyer || !$flyer->count()){
                throw new MarketPlaceException("Resource id:" . $id ." not found", 404);
            }


            $this->set('results', $flyer);
            $this->set('success', true);
            $this->set('code', 200);
        } catch (MarketPlaceException $ex) {
            throw $ex;   
        } catch (\Cake\Http\Exception\NotFoundException $ex) {
            throw new MarketPlaceException("Resource id:" . $id ." not found", 404);  
        } catch (Exception $ex) {
            throw new MarketPlaceException(
                    "Flyers generic error: " . $ex->getMessage(), 500);   
        }    
        
    }

    
    /**
     * default CRUD action
     */
    
    
    private $_disbleCrud = true;
    
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if($this->_disbleCrud){
            throw new MarketPlaceException("Not Allowed", 400);
        }
        
        $flyer = $this->Flyers->newEntity();
        if ($this->request->is('post')) {
            $flyer = $this->Flyers->patchEntity($flyer, $this->request->getData());
            if ($this->Flyers->save($flyer)) {
                $this->Flash->success(__('The flyer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The flyer could not be saved. Please, try again.'));
        }
        $this->set(compact('flyer'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Flyer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if($this->_disbleCrud){
            throw new MarketPlaceException("Not Allowed", 400);
        }
        $flyer = $this->Flyers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $flyer = $this->Flyers->patchEntity($flyer, $this->request->getData());
            if ($this->Flyers->save($flyer)) {
                $this->Flash->success(__('The flyer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The flyer could not be saved. Please, try again.'));
        }
        $this->set(compact('flyer'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Flyer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if($this->_disbleCrud){
            throw new MarketPlaceException("Not Allowed", 400);
        }
        
        $this->request->allowMethod(['post', 'delete']);
        $flyer = $this->Flyers->get($id);
        if ($this->Flyers->delete($flyer)) {
            $this->Flash->success(__('The flyer has been deleted.'));
        } else {
            $this->Flash->error(__('The flyer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
