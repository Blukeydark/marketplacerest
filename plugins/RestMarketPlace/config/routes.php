<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'RestMarketPlace',
    ['path' => '/rest-market-place'],
    function (RouteBuilder $routes) {
         // /rest-market-place/flyers.json list all flyers
         // /rest-market-place/flyers/{{id}}.json get by ID
        $routes->setExtensions(['json']);
        $routes->resources('Flyers');
    }
);
