# RestMarketPlace plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require RestMarketPlace
```


# OR edit you composer json and update project:
```
 "autoload": {
        "psr-4": {
            "App\\": "src/",
            "RestMarketPlace\\": "./plugins/RestMarketPlace/src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "App\\Test\\": "tests/",
            "Cake\\Test\\": "vendor/cakephp/cakephp/tests/",
            "RestMarketPlace\\Test\\": "./plugins/RestMarketPlace/tests/"
        }
    },
```

# Error handle
## Edit your config/app.php:

```
'Error' => [
        'errorLevel' => E_ALL,
        'exceptionRenderer' => \RestMarketPlace\Error\MarketPlaceExceptionRenderer::class,
        'skipLog' => [],
        'log' => true,
        'trace' => true,
    ],
```

## Put your CSV file in <project-path>/db/data/flyers_data.csv and run the command below:
```bash
root@72ac2f75aa97:/var/www# bin/cake migrations migrate
```
The mogrations create the table-entity "Flyers" and copy the CSV rows into table
